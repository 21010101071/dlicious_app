import 'dart:io';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class DmToListPage extends StatefulWidget {
  const DmToListPage({Key? key}) : super(key: key);

  @override
  State<DmToListPage> createState() => _DmToListPageState();
}

class _DmToListPageState extends State<DmToListPage> {
  List keypair = [
    {'num': '8160994731', 'text': 'Hello from pc dj'},
    {'num': '8238565870', 'text': 'hii from dj pc'},
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Center(
            child: Text(
              'Direct Message to list',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            ),
          ),
        ),
        body: Padding(
          padding: EdgeInsets.all(5),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TextButton(
                onPressed: () {
                  _launchWhatsapp(context);
                  // readFiler();
                },
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      margin: EdgeInsets.all(5),
                      child: Icon(
                        Icons.send,
                        color: Colors.green,
                      ),
                    ),
                    Text(
                      'click here to send message to list',
                      style: TextStyle(color: Colors.green),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  // readFiler() async {
  //   final input = new File('assets/data/DUEnrollmentNumber.csv').openRead();
  //   final fields = await input.transform(utf8.decoder).transform(new CsvToListConverter()).toList();
  //   print('abc :: $fields');
  // }

  _launchWhatsapp(context) async {
    for (int i = 0; i < keypair.length; i++) {
      var contact = keypair[i]['num'];
      var androidUrl = "whatsapp://send?phone=$contact&text=${keypair[i]['text']}";
      var iosUrl = "https://wa.me/$contact?text=${Uri.parse('${keypair[i]['text']}')}";

      try{
        if(Platform.isIOS){
          await launchUrl(Uri.parse(iosUrl));
        }
        else{
          await launchUrl(Uri.parse(androidUrl));
        }
      } on Exception{
        EasyLoading.showError('WhatsApp is not installed.');
      }
    }
  }
}

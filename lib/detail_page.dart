import 'package:flutter/material.dart';
import 'package:food_delivery/update_user.dart';

Widget getText(String txt) {
  return Text(txt);
}

class DetailPage extends StatefulWidget {
  Map map = {};

  DetailPage(this.map);

  @override
  State<DetailPage> createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          getText(widget.map['name']),
          getText(
            widget.map['price'].toString(),
          ),
          ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context){

                     return UpdateUser(widget.map);
                    },
                  ),
                );
              },
              child: Text('update'))
        ],
      ),
    );
  }
}

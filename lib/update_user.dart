import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

Widget getForm(TextEditingController t) {
  return TextFormField(
    controller: t,
  );
}

class UpdateUser extends StatefulWidget {
  TextEditingController _nameController = TextEditingController();
  TextEditingController _imgController = TextEditingController();

  Map? map = {};

  UpdateUser(this.map){
    print(map);
  }

  @override
  State<UpdateUser> createState() => _UpdateUserState();
}

class _UpdateUserState extends State<UpdateUser> {
  @override
  void initState() {
    if(widget.map!=null) {
      widget._nameController.text = widget.map!['name'];
      widget._imgController.text = widget.map!['image'];
    }
    else{
      widget._nameController.text = '';
      widget._imgController.text = "";
    }
    super.initState();
  }

  Future<http.Response> userUpdate(id) async {
    var res = await http.put(Uri.parse('https://63ef0a394d5eb64db0c228ac.mockapi.io/Food/$id'), body: widget.map);
    return res;
  }
  Future<http.Response> inserUser() async {
    Map map={};
    map['name']=widget._nameController.text;
    map['image']=widget._imgController.text;
    var res = await http.post(Uri.parse('https://63ef0a394d5eb64db0c228ac.mockapi.io/Food'), body:map);
    return res;
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        child: Column(
          children: [
            getForm(widget._nameController),
            getForm(widget._imgController),
            ElevatedButton(onPressed: ()  {
            print(widget.map);
              if(widget.map!=null) {
                widget.map!['name']=widget._nameController.text;
                widget.map!['image']=widget._imgController.text;
                 userUpdate(widget.map!['id']).then((value) => Navigator.pop(context));
              }
              else{
                print("object");
                 inserUser().then((value) => Navigator.pop(context));
              }
            }, child: Text("submit"))
          ],
        ),
      ),
    );
  }
}

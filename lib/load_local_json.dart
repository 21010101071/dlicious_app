import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:food_delivery/detail_page.dart';
import 'package:food_delivery/update_user.dart';
import 'package:http/http.dart' as http;

class LocalJsonParsing extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // await

    return Scaffold(
      appBar: AppBar(title: IconButton(onPressed: () =>Navigator.push(context, MaterialPageRoute(builder: (context) => UpdateUser(null),)) , icon: Icon(Icons.add))),
      backgroundColor: Colors.white,
      body: FutureBuilder<http.Response>(
        builder: (context, snapshot) {
          if(snapshot.hasData) {
            List<dynamic> l=jsonDecode(snapshot.data!.body);
            return ListView.builder(
            itemCount: jsonDecode(snapshot.data!.body.toString()).length ,
            itemBuilder: (context, index) {
              return InkWell(
                onTap: () {
                  Navigator.push(context,  MaterialPageRoute(builder:  (context) => DetailPage(l[index])
                  ));
                },
                child: Card(
                  child:
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child:
                    Row(
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Image.network( l[index]['image'].toString(),),
                              Text(
                                l[index]['name'].toString(),
                                style: TextStyle(color: Colors.black, fontSize: 20),
                              ),
                              Text(
                                l[index]['price'].toString(),
                                style: TextStyle(color: Colors.grey, fontSize: 15),
                              ),
                              Row(
                                children: [
                                  Text(
                                    l[index]['Rating'].toString(),
                                    style: TextStyle(color: Colors.orange, fontSize: 15),
                                  ),
                                  Icon(Icons.star_border)
                                ],
                              ),

                            ],
                          ),
                        ),
                        Icon(Icons.arrow_forward_rounded)
                      ],
                    ),

                  ),
                ),
              );
            },

          );
          } else{
            return Center(child: CircularProgressIndicator());
          }

        },
        future: getDataFromWebServer(),
      ),
    );
  }

  Future<http.Response> getDataFromWebServer() async {
    var response = await http
    .get(Uri.parse('https://63ef0a394d5eb64db0c228ac.mockapi.io/Food'));

    return response;
  }
}


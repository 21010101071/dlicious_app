import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:url_launcher/url_launcher.dart';

class DirectMessagePage extends StatefulWidget {
  @override
  State<DirectMessagePage> createState() => _DirectMessagePageState();
}

class _DirectMessagePageState extends State<DirectMessagePage> {
  TextEditingController formfiledcontroler = TextEditingController();
  TextEditingController numbercontroler = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Center(
            child: Text(
              'Direct Message',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            ),
          ),
        ),
        body: Padding(
          padding: EdgeInsets.all(5),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TextField(
                decoration:
                    InputDecoration(hintText: 'Enter Contact number here....'),
                keyboardType: TextInputType.phone,
                controller: numbercontroler,
              ),
              TextField(
                controller: formfiledcontroler,
              ),
              TextButton(
                onPressed: () {
                  launchWhatsapp(
                      formfiledcontroler.text, numbercontroler.text);
                },
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.send,
                      color: Colors.green,
                    ),
                    Text(
                      'click here',
                      style: TextStyle(color: Colors.green),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  launchWhatsapp(text, number) async {
    var contact = number;
    var androidUrl = "whatsapp://send?phone=$contact&text=${text}";
    var iosUrl = "https://wa.me/$contact?text=${Uri.parse('${text}')}";

    try{
      if(Platform.isIOS){
        await launchUrl(Uri.parse(iosUrl));
      }
      else{
        await launchUrl(Uri.parse(androidUrl));
      }
    } on Exception{
      EasyLoading.showError('WhatsApp is not installed.');
    }
  }

}
